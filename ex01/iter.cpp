/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 23:01:46 by triviere          #+#    #+#             */
/*   Updated: 2015/01/15 00:26:28 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template<typename T>
void		iter(T *array, int len, void (*fn)(T)) {
	for (int i = 0; i < len; ++i) {
		fn(array[i]);
	}
}

template<typename G>
void		print (G var) {
	std::cout << "Value: " << var << std::endl;
}



int			main () {
	int				numbers[5];
	std::string		strs[3];

	strs[0] = "Chaine 1";
	strs[1] = "Chaine 2";
	strs[2] = "Chaine 3";

	numbers[0] = 42;
	numbers[1] = 24;
	numbers[2] = 4242;
	numbers[3] = 2424;
	numbers[4] = 4422;

	iter(numbers, 5, &print);
	iter(strs, 3, &print);

	return (0);
}
