/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:23:38 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 19:02:50 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"
#include <iostream>

template <typename T, typename U>
void	each(T tab, int len, U fn)
{
	for (int i = 0; i < len; ++i)
		fn(tab[i]);
}

template <typename T>
void	print(T c)
{
	std::cout << "	" << c << std::endl;
}


int				main() {
	{
		Array<int>				a;
		Array<std::string>		b;

		std::cout << "[A] <int> " << a.size() << std::endl;
		std::cout << "[B] <std::string> " << b.size() << std::endl;
	}
	{
		Array<int>		a(5);
		Array<int>		b = a;

		for (int i = 0; i < 5; ++i)
			a[i] = i;
		std::cout << "[A] <int> -> LENGHT : " << a.size() << std::endl;
		each(a, a.size(), &print<int>);
		std::cout << "[B] <int> -> LENGHT : " << b.size() << std::endl;
		each(b, b.size(), &print<int>);
	}
	{
		Array<std::string> a(5);

		std::cout << "A <std::string> -> LENGTH : " << a.size() << std::endl;
	}
	return (0);
}
