/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 11:47:36 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 19:04:01 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __ARRAY_HPP__
# define __ARRAY_HPP__

# include <stddef.h>
# include <iostream>

template <typename T>
class Array {

	public:
		Array() : _array(0), _size(0) {}
		Array(Array const &obj) {
			int		len = obj.size();
			*this = obj;
			this->_array = new T[len];
			for (int i = 0; i < len; ++i) {
				this->_array[i] = obj._array[i];
			}
		}
		Array(unsigned int n) : _array(new T[n]), _size(n) {
			bzero(this->_array, sizeof(T) * n);
		}
		virtual ~Array() {}

		Array			&operator=(Array const &obj) {
			this->_size = obj._size;
			this->_array = new T[obj._size];
			for (int i = 0; i < obj._size; ++i)
				this->_array[i] = obj._array[i];
			return (*this);
		}

		int				size(void) const {
			return (this->_size);
		}

		T				&operator[](unsigned int n) const {
			unsigned int len = static_cast<unsigned int>(this->_size);
			if (n < len)
				return this->_array[n];
			throw std::out_of_range("Index not found");
		}

	private:
		T			*_array;
		int			_size;

};

#endif
